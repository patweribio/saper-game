#include "SFMLVIEW.h"
#include <SFML/Graphics.hpp>
#include <cstring>
#include <cerrno>


MSSFMLView::MSSFMLView (MinesweeperBoard & saperboard): board(saperboard)
{
    font.loadFromFile("bahnschrift.ttf");

    if (!font.loadFromFile("bahnschrift.ttf"))
    {
        std::cerr<< strerror(errno)<<std::endl;
        abort();
    }

    text.setFont(font);
    text.setCharacterSize(14);
    text.setPosition(board.getBoardWidth()*10/4, 15 + board.getBoardHeight()*10);

    mine_number.setFont(font);
    mine_number.setCharacterSize(9);
    mine_number.setColor(sf::Color::Color::White);

    board_field = sf::RectangleShape (sf::Vector2f(10, 10));
    board_field.setOutlineColor(sf::Color(157, 183, 191));
    board_field.setOutlineThickness(1.f);

    bomb.setRadius(5);
    bomb.setFillColor(sf::Color::Color::Red);

    flag.setRadius(5);
    flag.setPointCount(3);
    flag.setFillColor(sf::Color::Green);
}

void MSSFMLView:: drawOnWindow (sf::RenderWindow & game_window)
{

    int y_position = 0;
    int x_position = 0;

    for(int row=0; row<board.getBoardHeight(); row++)
    {
        y_position +=10;
        x_position = 0;

        for(int col=0; col<board.getBoardWidth(); col++)
        {
            x_position +=10;
            board_field.setPosition(x_position, y_position);
            check_field_clolor(board, row, col, board_field);
            game_window.draw(board_field);
            draw_field_content(board, row, col, game_window, x_position, y_position);
            show_mine_number(board, row, col, game_window, x_position, y_position);
        }
    }
}


void MSSFMLView::check_field_clolor(MinesweeperBoard &board, int &row, int &col, sf::RectangleShape &board_field)
{
    if (board.isRevealed(row, col))
        board_field.setFillColor(sf::Color::Color::Black);
    else
        board_field.setFillColor(sf::Color(135, 155, 161));
}


void MSSFMLView::draw_field_content(MinesweeperBoard &board, int &row, int &col, sf::RenderWindow &game_window, int x_position, int y_position)
{
    if (board.hasFlag(row, col))
    {
        flag.setPosition(x_position, y_position);
        game_window.draw(flag);
    }

    if (board.getFieldInfo(row,col) == 'x' && board.getGameState() == FINISHED_LOSS)
    {
        bomb.setPosition(x_position, y_position);
        game_window.draw(bomb);
        text.setString("You lose, try again.");
        text.setColor(sf::Color::Color::Red);
        game_window.draw(text);
    }

    if(board.getGameState() == FINISHED_WIN)
    {
        text.setString("You win, congratulation!");
        text.setColor(sf::Color::Color::Yellow);
        game_window.draw(text);
    }
}


void MSSFMLView::show_mine_number(MinesweeperBoard &board, int &row, int &col, sf::RenderWindow &game_window, int x_position, int y_position)
{
    char info = board.getFieldInfo(row, col);

    if (info>47 && info<57)
    {
        mine_number.setString(info);
        mine_number.setPosition(x_position+3, y_position-1);
        game_window.draw(mine_number);
    }
}