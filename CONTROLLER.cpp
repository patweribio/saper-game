#include "CONTROLLER.h"

using std::cin;
using std::cout;
using std::endl;

MSTextController::MSTextController(MinesweeperBoard &saperboard, MSBoardTextView &text_view): board(saperboard), view(text_view)
{

}

void MSTextController::GetCorrectCoordinate(int &row, int &col)
{
    cout<<"Input coordinate: "<<endl;

    do
    {
        cout<<"vertical: ";
        cin>>row;
    } while(row < 0 || board.getBoardHeight() < row);
    do
    {
        cout<<"horizontal: ";
        cin>>col;
    } while(row < 0 || board.getBoardWidth() < col);
}


void MSTextController::play()
{
    int akcja, row, col;

    do
    {
        cout<<"Choose option"<<endl;
        cout<<"1. Reveal field."<<endl<<"2. Toggle flag."<<endl;

        do
        {
            cout<<"Option: ";
            cin>>akcja;
        } while(akcja < 1 || akcja > 2);

        GetCorrectCoordinate(row, col);

        if (akcja == 1)
            board.revealField(row,col);
        else
            board.toggleFlag(row, col);

        view.text_display();

    } while(board.getGameState() == RUNNING);

    if (board.getGameState() == FINISHED_LOSS)
    {
        cout<<"You lose, try again.";
        abort ();
    }

    cout<<"You, win! Well done.";
}