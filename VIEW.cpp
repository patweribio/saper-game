#include <iostream>
#include "VIEW.h"
#include "MINESBOARD.h"

using std::cout;
using std::endl;

MSBoardTextView::MSBoardTextView(MinesweeperBoard & saperboard) : board(saperboard)
{

}

void MSBoardTextView::view_ColNumbers() const
{
    cout<<"  ";

    for (int col_number=0; col_number<board.getBoardWidth(); col_number++)
    {
        cout << " " << col_number;

        if (col_number<10)
            cout<< " ";
    }
    cout<<endl;
}


void MSBoardTextView::text_display() const
{
    int height = board.getBoardHeight();
    int width = board.getBoardWidth();

    view_ColNumbers();

    for(int row=0; row < height; row++)
    {
        cout<<row<<" ";

        for (int col=0; col < width; col++)
            cout << "[" << board.getFieldInfo(row, col) << "]";

        cout << "\n";
    }
}
