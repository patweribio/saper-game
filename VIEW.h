#ifndef SAPER_CALOSC_VIEW_H
#define SAPER_CALOSC_VIEW_H

#include "MINESBOARD.h"
#include <iostream>

class MSBoardTextView
{
   MinesweeperBoard & board;
   void view_ColNumbers() const;
public:
    MSBoardTextView(MinesweeperBoard & saperboard);
    void text_display() const;
};

#endif //SAPER_CALOSC_VIEW_H
