#ifndef SAPER_CALOSC_SFMLVIEW_H
#define SAPER_CALOSC_SFMLVIEW_H

#include <SFML/Graphics.hpp>
#include "MINESBOARD.h"

class MSSFMLView
{
    MinesweeperBoard &board;

    sf::Font font;
    sf::Text text;
    sf::Text mine_number;
    sf::RectangleShape board_field;
    sf::CircleShape bomb;
    sf::CircleShape flag;

    void check_field_clolor(MinesweeperBoard &board, int &row, int &col, sf::RectangleShape &board_field);
    void draw_field_content(MinesweeperBoard &board, int &row, int &col, sf::RenderWindow &game_window, int x_position, int y_position);
    void show_mine_number(MinesweeperBoard &board, int &row, int &col, sf::RenderWindow &game_window, int x_position, int y_position);
public:
    MSSFMLView(MinesweeperBoard & board);
    void drawOnWindow (sf::RenderWindow & game_window);

};



#endif //SAPER_CALOSC_SFMLVIEW_H
