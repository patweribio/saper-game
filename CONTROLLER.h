#ifndef SAPER_CALOSC_CONTROLLER_H
#define SAPER_CALOSC_CONTROLLER_H

#include "VIEW.h"

class MSTextController
{
    MinesweeperBoard & board;
    MSBoardTextView & view;

    void GetCorrectCoordinate(int &row, int &col);
public:
    MSTextController(MinesweeperBoard &saperboard, MSBoardTextView &text_view);
    void play();
};


#endif //SAPER_CALOSC_CONTROLLER_H
