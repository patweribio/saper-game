#ifndef SAPER_CALOSC_MINESBOARD_H
#define SAPER_CALOSC_MINESBOARD_H

#include<iostream>
#include "Array2D.h"

using std::cout;
using std::endl;

enum GameMode {DEBUG, EASY, NORMAL, HARD};
enum GameState {RUNNING, FINISHED_WIN, FINISHED_LOSS};


struct Field
{
    bool hasFlag;
    bool hasMine;
    bool isRevealed;
};

class MinesweeperBoard
{
    Array2D <Field> board {100, 100};
    GameMode mode;
    GameState state;
    int height;
    int width;
    int Mine_count;
    int UnrevealedFields_count;
    bool IsFirstAction_done ;

    bool IsPossibleToCreateBoard (const int &width, const int &height);
    void add_mines(const int &width, const int &height, const GameMode &mode);
    void DEBUG_fill_mines(const int &width, const int &height);
    void fill_with_random_mines(int mines_number);
    bool IsInsideBoard(int row, int col) const;
    void edge_secure (int &row, int &col, int &row_begin, int &row_end, int &col_begin, int &col_end) const;

public:
    MinesweeperBoard(int width, int height, GameMode mode);
    int getBoardWidth() const;
    int getBoardHeight() const;
    int getMineCount() const;
    int countMines (int row, int col) const;
    bool hasFlag (int row, int col) const;
    void toggleFlag (int row, int col);
    void revealField(int row, int col);
    bool isRevealed(int row, int col) const;
    GameState getGameState() const;
    char getFieldInfo(int row, int col) const;
    void debug_display() const;
};

#endif //SAPER_CALOSC_MINESBOARD_H
