#include "CONTROLLER_SFML.h"
#include <SFML/Graphics.hpp>

using std::cout;
using std::endl;

Board_Controller::Board_Controller(MinesweeperBoard &board, MSSFMLView &graphic_view) :board(board), graphic_view(graphic_view)
{

}

void Board_Controller::handleEvent( sf::Event &event, sf::RenderWindow &win, MinesweeperBoard &board)
{
    if (event.type == sf::Event::Closed)
        win.close();

    if (event.type == sf::Event::MouseButtonPressed)
    {
        int board_row = (event.mouseButton.y-10)/10;
        int board_col = (event.mouseButton.x-10)/10;

        if(event.mouseButton.button == sf::Mouse::Left)
            board.revealField(board_row, board_col);

        else if (event.mouseButton.button == sf::Mouse::Right)
            board.toggleFlag(board_row, board_col);
    }

    if(event.type == sf::Event::Resized)
        win.setView(sf::View(sf::FloatRect(0, 0, event.size.width, event.size.height)));
}