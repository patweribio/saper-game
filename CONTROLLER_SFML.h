#ifndef SAPER_CALOSC_CONTROLLER_SFML_H
#define SAPER_CALOSC_CONTROLLER_SFML_H

#include "SFMLVIEW.h"
#include <SFML/Graphics.hpp>

class Board_Controller
{
    MinesweeperBoard &board;
    MSSFMLView &graphic_view;

public:
    Board_Controller(MinesweeperBoard &board, MSSFMLView &graphic_view);
    void handleEvent(sf::Event &event, sf::RenderWindow &win, MinesweeperBoard &board);
};


#endif //SAPER_CALOSC_CONTROLLER_SFML_H
