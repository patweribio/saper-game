#include <iostream>
#include <SFML/Graphics.hpp>

#include "MINESBOARD.h"
#include "VIEW.h"
#include "CONTROLLER.h"
#include "SFMLVIEW.h"
#include "CONTROLLER_SFML.h"
#include "TESTS.h"

using namespace std;

int main()
{
//    test1();
//    test2();
//    test3();
//    test4();

//    MinesweeperBoard board(3, 3, DEBUG);
    MinesweeperBoard board(30, 40, DEBUG);
    MSSFMLView view (board);

    const int screen_width = 20 + board.getBoardWidth()*10;
    const int screen_height = 40 + board.getBoardHeight()*10;

    sf::RenderWindow win (sf::VideoMode(screen_width, screen_height), "SAPER Game");
    win.setVerticalSyncEnabled(true);

    Board_Controller ctrl(board, view);

//    test5(board );

    while (win.isOpen())
    {
        sf::Event event;

        while(win.pollEvent(event))
            ctrl.handleEvent(event, win, board);

        win.clear(sf::Color(156, 179, 186));
        view.drawOnWindow(win);
        win.display();
    }
    return 0;
}