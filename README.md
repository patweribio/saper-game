# Gra w sapera.

### Projekt wykonany w aplikacji Clion firmy JetBrains.


1. #### Informacje techniczne.

* Pliki SFML oraz MinGW użyte podczas tworzenia pobrane zostały ze strony: [Kliknij w link do strony z SFML 2.5.1](<https://www.sfml-dev.org/download/sfml/2.5.1/>).

![Windows 10 C disc](Screenshots/SFML_and_MinGW_version.png)

* Dla systemu Windows 10 wypakowane zostały w następującej lokalizacji.

![Windows 10 C disc](Screenshots/SFML_and_MinGW_location.png)

* Dodane zostały również zmienne globalne zgodnei z tym co widać na poniższym zrzucie.

![Windows 10 C disc](Screenshots/SFML_and_MinGW_enviroment_variables_win.png)

* Pliki czcionek wczytywane przez program znajdują się w folderze "cmake-build-debug".


2. #### Podgląd wyglądu gry w systemie Linux.

![Gameplay screenshot](Screenshots/Saper_board_0.png)
![Gameplay screenshot](Screenshots/Saper_board_1.png)
