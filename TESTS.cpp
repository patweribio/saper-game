#include "TESTS.h"

#include "MINESBOARD.h"
#include "VIEW.h"
#include "CONTROLLER.h"
#include "SFMLVIEW.h"

void test1 ()
{
    MinesweeperBoard e (10, 10, EASY);
    e.debug_display();
    cout<<endl;

    MinesweeperBoard n (11, 70, NORMAL);
    n.debug_display();
    cout<<endl;

    MinesweeperBoard h (1000, 1000, HARD);
    h.debug_display();
    cout<<endl;

    MinesweeperBoard u(100, 100, DEBUG);
    u.debug_display();
    cout<<endl;

}

void test2 ()
{
    MinesweeperBoard x (10, 10, DEBUG);
    x.debug_display();
    cout<<endl;

    cout<<"width: "<<x.getBoardWidth()<<endl;
    cout<<"height: "<<x.getBoardHeight()<<endl;
    cout<<"mine count: "<<x.getMineCount()<<endl;

    cout<<"mines around (0,0): "<<x.countMines (0,0) <<endl;
    cout<<"mines around (5,10): "<<x.countMines (5,10) <<endl;
    cout<<"mines around (10000,10000): "<<x.countMines (10000,10000) <<endl;

    cout<<"has flag (0,0)?: "<<x.hasFlag (0,0)<<endl;
    cout<<"has flag (5,10)?: "<<x.hasFlag (5,10) <<endl;
    cout<<"has flag (10000,10000)?: "<<x.hasFlag (10000,1000) <<endl;

    x.toggleFlag (0,0);
    x.toggleFlag (10000,10000);
    x.toggleFlag (-1,-1);

    cout<<"has flag (0,0)?: "<<x.hasFlag (0,0)<<endl;
    cout<<"has flag (5,10)?: "<<x.hasFlag (5,10) <<endl;

    cout<<"game state:"<<x.getGameState()<<endl;

    x.revealField(0,0);
    x.revealField(7,9);
    x.revealField(10000,10000);

    cout<<"is revealed (0,0)?: "<<x.isRevealed(0,0)<<endl;
    cout<<"is revealed (5,10)?: "<<x.isRevealed(5,10)<<endl;
    cout<<"is revealed (10000,10000)?: "<<x.isRevealed(10000,10000)<<endl;

    cout<<"game state:"<<x.getGameState()<<endl;

    cout<<"field (0,0) info:"<<x.getFieldInfo(0,0)<<endl;
    cout<<"field (7,9) info:"<<x.getFieldInfo(7,9)<<endl;
    cout<<"field (10000,10000) info:"<<x.getFieldInfo(10000,10000)<<endl;
}


void test3 ()
{
    MinesweeperBoard test_board (20, 10, EASY);
    test_board.debug_display();
    cout<<endl;
    MSBoardTextView view ( test_board );
    view.text_display();
    test_board.revealField(0,5);
    test_board.toggleFlag(-1,-1);
    test_board.revealField(0,0);
    test_board.toggleFlag(7,5);
    cout<<endl;
    view.text_display();
}

void test4()
{
    MinesweeperBoard board (20, 10, EASY);
    board.debug_display();
    cout<<endl;
    MSBoardTextView view (board);
    MSTextController ctrl (board, view);
    ctrl.play();
}

void test5(MinesweeperBoard &board)
{
    board.revealField(1,0);
    board.revealField(1,2);
    board.revealField(2,1);
}