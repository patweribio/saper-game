#include <iostream>
#include <cstdlib> //dla rand i srand
#include <ctime>
#include <cmath>
#include "MINESBOARD.h"

using std::cout;
using std::endl;

MinesweeperBoard::MinesweeperBoard (int width, int height, GameMode mode)
{
    if (!IsPossibleToCreateBoard (width, height))
        abort();

    state = RUNNING;
    IsFirstAction_done = false;
    UnrevealedFields_count = width*height;

    this->width = width;
    this->height = height;

    for (int row=0; row<height; row++)
    {
        for (int col=0; col<width; col++)
        {
            board[row][col].hasMine = false;
            board[row][col].isRevealed = false;
            board[row][col].hasFlag = false;
        }
    }

    add_mines(width,height, mode);
}


bool MinesweeperBoard::IsPossibleToCreateBoard (const int &width, const int &height)
{
    if (width>100 || width<0 ||height>100 || height<0)
    {
        cout<<"Wrong size of the board (have to be >0 and max: 100 rows and 100 cols)";
        return 0;
    }
    return 1;
}

void MinesweeperBoard::add_mines(const int &width, const int &height, const GameMode &mode)
{
    switch (mode)
    {
        case EASY:
            Mine_count = round(0.1*width*height);
            fill_with_random_mines(Mine_count);
            break;
        case NORMAL:
            Mine_count = round(0.2*width*height);
            fill_with_random_mines(Mine_count);
            break;
        case HARD:
            Mine_count = round(0.3*width*height);
            fill_with_random_mines(Mine_count);
            break;
        case DEBUG:
            DEBUG_fill_mines(width, height);
            break;
        default:
            cout <<"Failed adding mines, wrong mode, try again!"<<endl;
            break;
    }
}


void MinesweeperBoard::DEBUG_fill_mines(const int &width, const int &height)
{
    Mine_count = 0;

    for (int row=0; row<height; row++)
    {
        for (int col=0; col<width; col++)
        {
            if (col==row || row == 0 || (col==0 && row%2==0))
            {
                board[row][col].hasMine = true;
                Mine_count ++;
            }
        }
    }
}



void MinesweeperBoard::fill_with_random_mines(int mines_number)
{
    srand (time(NULL));

    while (mines_number>0)
    {
        int row = rand()%height;
        int col = rand()%width;

        if (!board[row][col].hasMine)
        {
            board[row][col].hasMine = true;
            mines_number--;
        }
    }
}


void MinesweeperBoard::debug_display() const
{
    for (int row=0; row<height; row++)
    {
        for (int col=0; col<width; col++)
        {
            cout<<"[";

            if (board[row][col].hasMine)
                cout<<"M";
            else
                cout<<".";

            if (board[row][col].isRevealed)
                cout<<"o";
            else
                cout<<".";

            if (board[row][col].hasFlag)
                cout<<"f";
            else
                cout<<".";

            cout<<"]";
        }
        cout<<endl;
    }
}


int MinesweeperBoard::getBoardWidth() const
{
    return width;
}


int MinesweeperBoard::getBoardHeight() const
{
    return height;
}


int MinesweeperBoard::getMineCount() const
{
    return Mine_count;
}


bool MinesweeperBoard::IsInsideBoard(int row, int col) const
{
    if (col>=width || col<0 || row>=height || row<0)
        return false;
    return true;
}


void MinesweeperBoard::edge_secure (int &row, int &col, int &row_begin, int &row_end, int &col_begin, int &col_end) const
{
    if(!IsInsideBoard(row-1,col))
        row_begin = row;
    else
        row_begin = row-1;

    if(!IsInsideBoard(row+1,col))
        row_end = row;
    else
        row_end = row+1;

    if(!IsInsideBoard(row,col-1))
        col_begin = col;
    else
        col_begin = col-1;

    if(!IsInsideBoard(row,col+1))
        col_end = col;
    else
        col_end = col+1;
}


int MinesweeperBoard::countMines (int row, int col) const
{
    int mines_number = 0;

    if (!IsInsideBoard(row,col) || !board[row][col].isRevealed)
        return -1;

    int r_begin, r_end, c_begin, c_end;
    edge_secure(row, col, r_begin, r_end, c_begin, c_end);

    for (int i=r_begin; i<=r_end; i++)
    {
        for (int j=c_begin; j<=c_end; j++)
        {
            if(board[i][j].hasMine)
                mines_number++;
        }
    }

    return mines_number;
}


bool MinesweeperBoard::hasFlag (int row, int col) const
{
    if (IsInsideBoard(row,col) && !board[row][col].isRevealed && board[row][col].hasFlag)
        return true;
    return false;
}


void MinesweeperBoard::toggleFlag (int row, int col)
{
    if (IsInsideBoard(row, col) && !board[row][col].isRevealed && state == RUNNING)
        board[row][col].hasFlag = true;
}

void MinesweeperBoard::revealField(int row, int col)
{
    if (IsInsideBoard(row,col) && !board[row][col].isRevealed && !board[row][col].hasFlag && state == RUNNING )
    {
        board[row][col].isRevealed = true;
        UnrevealedFields_count--;

        if (board[row][col].hasMine)
        {
            if (!IsFirstAction_done)
            {
                board[row][col].hasMine = false;
                fill_with_random_mines(1);
            }
            else
                state = FINISHED_LOSS;
        }

        if (!IsFirstAction_done)
            IsFirstAction_done = true;
    }
}

bool MinesweeperBoard::isRevealed(int row, int col) const
{
    if (IsInsideBoard(row,col) && board[row][col].isRevealed)
        return true;
    return false;
}

GameState MinesweeperBoard::getGameState() const
{
    if (state == FINISHED_LOSS)
        return FINISHED_LOSS;

    if (UnrevealedFields_count == Mine_count)
        return FINISHED_WIN;

    return RUNNING;
}

char MinesweeperBoard::getFieldInfo(int row, int col) const
{
    if (!IsInsideBoard(row, col))
        return '#';

    if (!board[row][col].isRevealed && board[row][col].hasFlag)
        return 'F';

    if (!board[row][col].isRevealed && !board[row][col].hasFlag)
        return '_';

    if (board[row][col].isRevealed && board[row][col].hasMine)
        return 'x';

    if (board[row][col].isRevealed && countMines(row, col) == 0)
        return ' ';

    return (48 + countMines(row, col));    /* wykona sie gdy board[row][col].isRevealed && countMines(col, row)!=0 */
}